#ifndef _MAIN_H_
#define _MAIN_H_

#include <gtk/gtk.h>
#include "treefolderview.h"
#include "context.h"
#include "interactor.h"

GtkWidget  *main_window;
GtkWidget  *d_area;
GtkWidget  *source_view;
GtkWidget  *tf_view;
GtkWidget  *tbtn_play;

GtkComboBox     *cb_actions;
GtkComboBoxText *cb_directions;
GtkListStore    *store_actions;
GtkAdjustment	*zoom_adj;

SpriteContext *context;
Interactor    *interactor;

gchar *
get_action_id (gint hp, const gchar *name);

gchar *
buffer_get_text ();

void
buffer_set_modified (gboolean modified);

void
release_context ();

void
intr_updated (Interactor *interactor);

gchar *
get_opened_file_name ();


#endif

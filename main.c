#include <gtksourceview/gtksource.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include "xml.h"
#include "config.h"

#include "main.h"

gchar         *opened_file_name = NULL;

gchar *
get_action_id (gint hp, const gchar *name) {
	return g_strdup_printf ("%d:%s", hp, name);
}

void
buffer_mark_line (gint line_no) {
	GtkTextIter iter, end;
	GtkSourceBuffer *buffer = GTK_SOURCE_BUFFER (
		gtk_text_view_get_buffer (GTK_TEXT_VIEW (source_view))
	);

	gtk_text_buffer_get_start_iter (GTK_TEXT_BUFFER (buffer), &iter);
	gtk_text_buffer_get_end_iter   (GTK_TEXT_BUFFER (buffer),  &end);

	gtk_source_buffer_remove_source_marks (
		buffer,
		&iter,
		&end,
		"active-line"
	);

	if (line_no == -1)
		return;

	gtk_text_iter_set_line (&iter, line_no);
	gtk_text_view_scroll_to_mark (
		GTK_TEXT_VIEW (source_view),
		(GtkTextMark *) gtk_source_buffer_create_source_mark (
			buffer,
			NULL, 
			"active-line", 
			&iter
		),
		0.0,
		TRUE,
		0.0,
		0.5
	);
}

void
intr_updated (Interactor *interactor) {
	gtk_widget_queue_draw (d_area);

	gtk_tool_button_set_icon_name (
		GTK_TOOL_BUTTON (tbtn_play),
		interactor_loop_running (interactor) ?
			"media-playback-pause" :
			"media-playback-start"
	);

	if (interactor != NULL) {
		gboolean result;
		gint hp;
		gchar *id, *name;

		buffer_mark_line (
			interactor_get_line_no (interactor) - 1
		);

		result = interactor_get_action_hp_and_name (
			interactor,
			&hp, &name
		);

		if (!result)
			return;

		id = get_action_id (hp, name);
		gtk_combo_box_set_active_id (cb_actions, id);
		g_free (id);

		gtk_combo_box_set_active_id (
			GTK_COMBO_BOX (cb_directions),
			interactor_get_animation_direction (interactor)
		);
	} else
		buffer_mark_line (-1);
}

void
release_context () {
	if (interactor != NULL) {
		interactor_free (interactor);
		interactor = NULL;
		intr_updated (interactor);
	}

	gtk_combo_box_text_remove_all (cb_directions);
	gtk_list_store_clear (store_actions); /* TODO: free strings */
}

void
file_activated_cb (
	TreeFolderView *tfview,
	gchar *filename,
	gpointer user_data
) {
	gsize len;
	gchar *text;
	GtkSourceView *view;

	release_context ();

	g_return_if_fail (GTK_SOURCE_IS_VIEW (user_data));
	view = GTK_SOURCE_VIEW (user_data);

	g_file_get_contents (
		filename,
		&text,
		&len,
		NULL
	);

	gtk_text_buffer_set_text (
		gtk_text_view_get_buffer (GTK_TEXT_VIEW (view)),
		text, len
	);

	gtk_text_buffer_set_modified (
		gtk_text_view_get_buffer (GTK_TEXT_VIEW (view)),
		FALSE
	);

	gtk_window_set_title (GTK_WINDOW (main_window), filename);
	opened_file_name = g_strdup (filename);
}

gboolean
draw_callback (
	GtkWidget *d_area,
	cairo_t *cr,
	gpointer user_data
) {
	const GdkPixbuf *sprite;
	gint offsetX, offsetY;

	gint width = gtk_widget_get_allocated_width (d_area);
	gint height = gtk_widget_get_allocated_height (d_area);
	
	gdouble scale = gtk_adjustment_get_value (zoom_adj);

	cairo_translate (cr, width / 2, height / 2);
	cairo_scale (cr, scale, scale);

	if (interactor == NULL)
		return FALSE;

	sprite = interactor_get_sprite (interactor);

	if (sprite == NULL)
		return FALSE;

	interactor_get_offset (interactor, &offsetX, &offsetY);

	gdk_cairo_set_source_pixbuf (
		cr, sprite,
		offsetX,
		offsetY
	);
	cairo_paint(cr);

	return FALSE;
}

void
setup_source_view (GtkSourceView *source_view) {
	GtkSourceLanguageManager *langman;
	GtkSourceMarkAttributes  *attrs;
	
	langman = gtk_source_language_manager_get_default();

	gtk_source_buffer_set_language (
		GTK_SOURCE_BUFFER (
			gtk_text_view_get_buffer (
				GTK_TEXT_VIEW (source_view)
			)
		),
		gtk_source_language_manager_get_language (langman, "xml")
	);

	attrs = gtk_source_mark_attributes_new ();
	gtk_source_mark_attributes_set_icon_name (attrs, "media-record");
	gtk_source_view_set_mark_attributes (
		source_view, "active-line", attrs, 0 
	);

	gtk_widget_show_all (GTK_WIDGET (source_view));
}

gchar *
buffer_get_text () {
	GtkTextIter start, end;
	GtkTextBuffer *buffer =
		GTK_TEXT_BUFFER (gtk_text_view_get_buffer (
			GTK_TEXT_VIEW (source_view)
		));

	gtk_text_buffer_get_start_iter (buffer, &start);
	gtk_text_buffer_get_end_iter (buffer, &end);

	return gtk_text_buffer_get_text (buffer, &start, &end, TRUE);
}

void
buffer_set_modified (gboolean modified) {
	GtkTextBuffer *buffer =
		GTK_TEXT_BUFFER (gtk_text_view_get_buffer (
			GTK_TEXT_VIEW (source_view)
		));

	gtk_text_buffer_set_modified (buffer, modified);
}

GtkWidget *
gtk_builder_get_widget (
	GtkBuilder *builder,
	const gchar *name
) {
	return GTK_WIDGET (
		gtk_builder_get_object (builder, name)
	);
}

gchar *
get_opened_file_name () {
	return opened_file_name;
}

int
main (int argc, char *argv[]) {
	GtkBuilder *builder;
    char datadir[5000];

	gtk_init (&argc, &argv);
	tree_folder_view_get_type();

	builder = gtk_builder_new ();
    printf("--------------------");
    printf("Final: %s\n", argv[0]);

    strncpy(datadir, argv[0], sizeof(datadir));
    dirname(datadir);
    if (!strncmp(datadir, "saedit", 6)) {
        strcpy(datadir, getenv("_")); // FIXME: Not reliable, but BASH should work fine.
        dirname(datadir);
    }
    if (!strncmp(datadir, "saedit", 6)) {
        strcpy(datadir, "/usr/local/bin/");
        printf("[WARNING] env variable '_' is unknown, defaulting path to /usr/local/bin\n");
    }
    strncat(datadir, "/iface.ui", sizeof(datadir));

    
    printf("Data dir: %s\n", datadir);
    printf("ENV: %s\n", getenv("_"));
    printf("--------------------\n");
    

	gtk_builder_add_from_file (builder, datadir, NULL);
	gtk_builder_connect_signals (builder, NULL);

	main_window   = gtk_builder_get_widget (builder,         "window-main");
	tf_view       = gtk_builder_get_widget (builder, "treefolderview-main");
	d_area        = gtk_builder_get_widget (builder,    "drawingarea-main");
	source_view   = gtk_builder_get_widget (builder,     "sourceview-main");
	tbtn_play     = gtk_builder_get_widget (builder,    "button-anim-play");

	cb_actions    = GTK_COMBO_BOX (
		gtk_builder_get_widget (builder,     "cbox-actions")
	);
	cb_directions = GTK_COMBO_BOX_TEXT (
		gtk_builder_get_widget (builder,  "cboxtext-directions")
	);

	store_actions = GTK_LIST_STORE (
		gtk_builder_get_object (builder, "liststore-actions")
	);

	zoom_adj = GTK_ADJUSTMENT (
		gtk_builder_get_object (builder, "zoom-adjustment")
	);

	setup_source_view (GTK_SOURCE_VIEW (source_view));

	g_object_unref (builder);

	tree_folder_view_set_filename (
		TREE_FOLDER_VIEW (tf_view),
		/* TODO: free it! */
		config_keys_get_data_folder_path ()
	);

	g_signal_connect (G_OBJECT (d_area), "draw", G_CALLBACK (draw_callback), NULL);

	gtk_widget_show_all (main_window);

	gtk_main();
	
	return 0;
}

#include "main.h"
#include "errors.h"
#include "config.h"
#include <string.h>

void
store_append_action (const Action *action, gpointer user_data) {
	int hp;
	gchar *name;
	GtkTreeIter iter;

	action_get_hp_and_name (action, &hp, &name);
	gtk_list_store_append (store_actions, &iter);

	gtk_list_store_set (
		store_actions,
		&iter,
		0, name,
		1, hp,
		2, get_action_id (hp, name),
		-1
	);
}

void
parse_buffer_clicked_cb (
	GtkToolButton *button,
	gpointer user_data
) {
	gchar *buffer_text;
	GError *error = NULL;
	GList *actions;

	release_context ();

	buffer_text = buffer_get_text ();

	context = sprite_context_new (tree_folder_view_get_filename (
		TREE_FOLDER_VIEW (tf_view)
	));

	sprite_context_add_sprite (
		context,
		xml_parse_buffer (buffer_text, &error),
		-1
	);

	g_free (buffer_text);

	interactor = interactor_new (context);
	interactor_set_updated_callback (interactor, intr_updated);

	actions = sprite_context_get_actions (context);
	g_list_foreach (actions, (GFunc) store_append_action, NULL);
	g_list_free (actions);
}

void
append_direction (const gchar *direction, gpointer user_data) {
	gchar *display;

	g_return_if_fail (direction != NULL);

	if (strlen (direction) == 0)
		display = g_strdup ("<empty>");
	else
		display = g_strdup (direction);

	gtk_combo_box_text_append (
		cb_directions,
		direction,
		display
	);

	g_free (display);
}

void
action_changed_cb (GtkComboBox *cbox, gpointer user_data) {
	GList *directions;
	GtkTreeIter iter;
	gint hp;
	gchar *name;
	const Action *action;
	gboolean w_run;

	g_return_if_fail (cbox == cb_actions);

	if (interactor == NULL)
		return;

	if (!gtk_combo_box_get_active_iter (cbox, &iter))
		return;

	gtk_tree_model_get (
		GTK_TREE_MODEL (store_actions),
		&iter,
		0, &name,
		1, &hp,
		-1
	);

	w_run = interactor_loop_running (interactor);
	if (w_run)
		interactor_loop_stop (interactor);

	gtk_combo_box_text_remove_all (cb_directions);

	action = sprite_context_get_action (context, hp, name);
	g_return_if_fail (action != NULL);

	interactor_set_action (interactor, hp, name);

	directions = action_get_directions (action);
	g_list_foreach (directions, (GFunc) append_direction, NULL);
	g_list_free (directions);

	if (w_run)
		interactor_loop_start (interactor, 10, 10);
}

void
direction_changed_cb (GtkComboBox *cbox, gpointer user_data) {
	g_return_if_fail (cbox == GTK_COMBO_BOX (cb_directions));

	if (interactor == NULL)
		return;

	interactor_set_direction (
		interactor,
		gtk_combo_box_text_get_active_text (cb_directions)
	);
}

void 
play_pause_clicked_cb (
	GtkToolButton *button,
	gpointer user_data
) {
	if (interactor == NULL)
		return;

	if (interactor_loop_running (interactor))
		interactor_loop_stop (interactor);
	else
		interactor_loop_start (interactor, 10, 10);
}

void 
next_frame_clicked_cb (
	GtkToolButton *button,
	gpointer user_data
) {
	if (interactor == NULL)
		return;

	interactor_skip_current_frame (interactor);
}

void 
first_frame_clicked_cb (
	GtkToolButton *button,
	gpointer user_data
) {
	if (interactor == NULL)
		return;

	interactor_loop_stop (interactor);
	interactor_reset_animation (interactor);
}

void
choose_df_activated_cb (
	GtkMenuItem *item,
	gpointer user_data
) {
	TreeFolderView *tfview;
	GtkWidget *dialog;
	gint res;
	gchar *filename;

	g_return_if_fail (IS_TREE_FOLDER_VIEW (user_data));

	tfview = TREE_FOLDER_VIEW (user_data);
	dialog = gtk_file_chooser_dialog_new (
		"Choose data folder",
		GTK_WINDOW (main_window),
		GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
		"Cancel", GTK_RESPONSE_CANCEL,
		"Open", GTK_RESPONSE_ACCEPT,
		NULL
	);

	gtk_file_chooser_set_filename (
		GTK_FILE_CHOOSER (dialog),
		tree_folder_view_get_filename (tfview)
	);

	res = gtk_dialog_run (GTK_DIALOG (dialog));

	if (res == GTK_RESPONSE_ACCEPT) {
		filename = gtk_file_chooser_get_filename (
			GTK_FILE_CHOOSER (dialog)
		);

		config_keys_set_data_folder_path (filename);
		config_keys_save ();

		tree_folder_view_set_filename (
			tfview,
			filename
		);

		g_free (filename);
	}
		
	gtk_widget_destroy (dialog);
}

void
save_file_activated_cb (
	GtkMenuItem *item,
	gpointer user_data
) {
	gchar *text, *filename;
	GError *error = NULL;
	gboolean success;

	filename = get_opened_file_name ();

	if (filename == NULL)
		return;

	text = buffer_get_text ();

	success = g_file_set_contents (
		filename,
		text,
		strlen (text),
		&error
	);

	if (!success) {
		post_error ("Saving", error->message);
	} else {
		buffer_set_modified (FALSE);
	}
}

void
zoom_adjustment_value_changed_cb (
	GtkAdjustment *adjustment,
	gpointer user_data
) {
	gtk_widget_queue_draw (GTK_WIDGET (user_data));
}


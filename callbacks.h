#ifndef _CALLBACKS_H_
#define _CALLBACKS_H_

void
parse_buffer_clicked_cb (
	GtkToolButton *button,
	gpointer user_data
);

void
action_changed_cb (GtkComboBox *cbox, gpointer user_data);

void
direction_changed_cb (GtkComboBox *cbox, gpointer user_data) {

void 
play_pause_clicked_cb (
	GtkToolButton *button,
	gpointer user_data
);

void 
first_frame_clicked_cb (
	GtkToolButton *button,
	gpointer user_data
);

void
choose_df_activated_cb (
	GtkMenuItem *item,
	gpointer user_data
);

void
save_file_activated_cb (
	GtkMenuIterm *item,
	gpointer user_data
);

void
zoom_adjustment_value_changed_cb (
	GtkAdjustment *adjustment,
	gpointer user_data
);

#endif

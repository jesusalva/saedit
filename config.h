#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <glib.h>

void
config_keys_load ();

void
config_keys_save ();

gchar *
config_keys_get_data_folder_path ();

void
config_keys_set_data_folder_path (
	const gchar *filename
);

void
config_data_paths_load ();

gchar *
config_data_paths_get_sprites_path ();

#endif

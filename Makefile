PREFIX = /usr/local

CFLAGS += `pkg-config --cflags gtk+-3.0 gtksourceview-3.0`
CFLAGS += -fPIC -Itreefolderview 
CFLAGS += -Wall -Wdeclaration-after-statement -ansi

LDFLAGS += `pkg-config --libs gtk+-3.0 gtksourceview-3.0`
LDFLAGS += -rdynamic -Ltreefolderview

FLAGS = ${CFLAGS} ${LDFLAGS}

all: saedit glade/libsaedit.so

saedit: main.o treefolderview/treefolderview.o xml.o  \
	context.o imageset.o action.o animation.o common.o \
	interactor.o callbacks.o errors.o config.o
	gcc $^ -o saedit ${FLAGS}

glade/libsaedit.so: treefolderview/treefolderview.o
	gcc $^ -o glade/libsaedit.so ${FLAGS} -shared

%.o: %.c
	gcc $^ -c -o $@ ${CFLAGS}

clean:
	rm -f *.o */*.o *~ glade/libsaedit.so saedit


.PHONY: install
install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp saedit $(DESTDIR)$(PREFIX)/bin/saedit
	cp iface.ui $(DESTDIR)$(PREFIX)/bin/iface.ui

.PHONY: uninstall
uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/saedit
	rm -f $(DESTDIR)$(PREFIX)/bin/iface.ui


